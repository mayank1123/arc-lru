
#include <unordered_map>
#include <bits/stdc++.h>
//#define c 32764

using namespace std ; 

typedef struct Node {
	unsigned int page;
	Node *next;
	Node *previous;
} Node ;

	
class hashtable {
	std::unordered_map<unsigned int, Node *> map;

public : 

	void add(unsigned int page, Node *page_addr){
		map[page] = page_addr;
	}

	void remove(unsigned int page){
		map.erase(page);
	}

	Node *get_addr(unsigned int page){
		try{ return map[page] ; }
		catch (exception& e) {return 0; }
	}
};


/* Global head and tail*/

int p ; // Parameter to control the size of T1 and thus T2 
int T1_size , B1_size , T2_size , B2_size ;
int size;
hashtable T1_hash , T2_hash , B1_hash , B2_hash ; 
struct Node *T1_head , *T2_head , *T1_tail , *T2_tail , *B1_head , *B2_head , *B1_tail , *B2_tail;
int page, num_of_pages;
int delta1;

void pop_node(Node *node){
/* Remove node from the middle */
	if(node->previous!=NULL) node->previous->next = node->next;
	if (node->next != NULL) node->next->previous = node->previous; 
	node->next = NULL;
	node->previous = NULL;
}

void push_node(Node *node1, Node *node2){
	/* push node1 at the front of node2 */
	if(node2 != NULL) {
	node2->next = node1;
	node1->previous = node2;
	node1->next = NULL;
	}
}

void create_node( unsigned int page, Node *node){
	/* create a new node in front of *node*/
	Node *node1= new(Node);
	node1->page = page;
	node1->previous = node;
	node1->next = NULL;
	node->next = node1;
}

void remove_node(Node *node){
	/* remove last node */
	node->next->previous =NULL;
	delete (node);
}

void print_list(){
	Node *tmp;
		tmp = B1_tail;  
		while(tmp != NULL) {
			cout << tmp->page << "->" ;
			tmp = tmp->next;   
		}
		cout << endl;
}

void replace(unsigned int xt , int p){
	/* Function to replace  */ 
	if(  (T1_size)  &&  ((T1_size>p) || (B2_hash.get_addr(xt) && T1_size == p))  ) {
	//	cout <<"level 0 replace" << endl;
		T1_hash.remove(T1_tail->page);
		B1_hash.add(T1_tail->page , T1_tail);
	//	cout << "Value sent to B1 is : " << T1_tail->page <<endl;
		B1_size++;
		T1_size--;
		Node *tmp = T1_tail;
	//	if(T1_tail == T1_head) cout <<"EUREKA !!!! " << endl;
		T1_tail = T1_tail->next;
		
		if(T1_tail == NULL) {T1_head = T1_tail; /*cout << "T1_tail is NULL" << endl ;*/ }
		else T1_tail->previous = NULL;
		
		if (B1_head!=NULL) { 
			push_node(tmp,B1_head);
			B1_head = tmp;
			B1_head->next = NULL;
		}
		else {
	
			 B1_head = tmp;
			 B1_head->next = NULL;
			 B1_head->previous = NULL;
			 B1_tail = B1_head;
		}

	}
	else {
		if (T2_tail==NULL) cout << "NULL TAIL" << endl;
		
		T2_hash.remove(T2_tail->page);

		B2_hash.add(T2_tail->page , T2_tail);

		B2_size++;
		T2_size--;
		Node *tmp = T2_tail;
		T2_tail = T2_tail->next;
		
		if(T2_tail == NULL) T2_head = T2_tail;
		else T2_tail->previous = NULL;
 
		if (B2_head!=NULL) {
		
			push_node(tmp,B2_head);
			B2_head = tmp;
			B2_head->next = NULL;
		}
		else{
			B2_head = tmp;
			B2_head->next = NULL;
			B2_head->previous = NULL;
			B2_tail = B2_head;
		}
	}
}

int main(int argc, char* argv[]){
	
	int c;
	int i;
	unsigned long hit;
	unsigned long miss;
	int xt;
	int crap1; 
	int crap2;
	string path;
	p = 0; 
	T1_size = 0;
	T2_size = 0;
	B1_size = 0;
	B2_size = 0;
	size = 0; 
	hit = 0; 
	miss = 0;
	c = 32768;
	if (argc > 2) 
	{ c = (unsigned int)atoi(argv[2]); } 
	if (argc > 1)
	{ path = argv[1];}
	std::ifstream input (path);
	
	T1_head = NULL; T2_head = NULL; T1_tail = NULL; T2_tail = NULL; B1_head = NULL; B2_head = NULL; B1_tail = NULL; B2_tail = NULL;
	
	while(input >> page >> num_of_pages >> crap1 >> crap2){
	
	//cout << "Main loop entered " << endl;
	
		for ( i =0 ; i<num_of_pages ; i++){
	
			xt = page;
	
		/* Case 1 : If page in T1 or T2 */
	
			if(T1_hash.get_addr(page)){
		
				Node *tmp = T1_hash.get_addr(page);
				hit++;
				/* Remove node from T1 and add to T2 head */ 
				if(tmp == T1_head) { T1_head = T1_head->previous ; /*cout << "Tmp is head" << endl;*/}
				if(tmp == T1_tail) { T1_tail = T1_tail->next; /*cout << "Tmp is tail" << endl;*/}
			
				pop_node(tmp);
		
				if(T2_head == NULL){      
					T2_head = tmp;
					T2_tail = T2_head;
				}
				else{
					push_node(tmp,T2_head);
					T2_head = tmp;
				}
				T2_hash.add(page,tmp);
				T1_hash.remove(page);
				T1_size--;
				T2_size++;
			}
	
			else if(T2_hash.get_addr(page)){
			
				hit++;
				Node *tmp;
				tmp = T2_hash.get_addr(page);
	
				if (T2_size>1 && T2_head !=tmp){
	
				/* Remove node from T2 and put at T2 head */
				
				if(T2_tail == tmp)  {
					T2_tail = T2_tail->next;
				}
				pop_node(tmp);
				push_node(tmp,T2_head);
				T2_head = tmp; 
				}
			}
	
		/* Case 2 : If page in B1 */
	
			else if(B1_hash.get_addr(page)){
		
				miss++;
				/* Update p */ 
				if (B1_size<B2_size && B1_size !=0) delta1 = (int)(B2_size/B1_size);
				else delta1 = 1; 
				p = min(p+delta1,c);
				replace(xt,p); 
	
				Node *tmp = B1_hash.get_addr(page);
	
				T2_hash.add(page,tmp);
				B1_hash.remove(page);
				T2_size++;
				B1_size--;
				
				/* Remove node from B1 and put at T2 head */
				if (B1_tail == tmp) B1_tail = B1_tail->next;
				if (B1_head == tmp) B1_head = B1_head->previous;
				pop_node(tmp);
				if(T2_head == NULL){
					T2_head = tmp;
					T2_tail = T2_head;
				}
				else{
					push_node(tmp, T2_head);
					T2_head = tmp;
				}
				
			}
		/* Case 3 : If page in B2 */ 
	
			else if(B2_hash.get_addr(page)){
		
				miss++;
	
				/* Update p */
				if (B1_size>B2_size && B2_size!=0) delta1 = (int)(B1_size/B2_size);
				else delta1 = 1; 
				p = max(p-delta1,0);
				replace(xt,p);
	
				Node *tmp = B2_hash.get_addr(page);
	
				T2_hash.add(page,tmp);
				T2_size++;
				B2_hash.remove(page);
				B2_size--;
	
				/* Remove node from B2 and put at T2 head */
				if (B2_tail == tmp) B2_tail = B2_tail->next;
				if (B2_head == tmp) B2_head = B2_head->previous;
				pop_node(tmp);
				if(T2_head == NULL){
					T2_head = tmp;
					T2_tail = T2_head;
				}
				else{
					push_node(tmp, T2_head);
					T2_head = tmp;
				}
			}
	
		/* Case 4 : If page not in Top or bottom caches */
	
			else {
		
				miss++;
	
			/* Case A : If cache already filled */
				
				if(T1_size + B1_size == c){
					
					if(T1_size < c){
		
						
						Node *tmp = B1_tail;
						B1_hash.remove(tmp->page);
						B1_size--;
						
						/* Delete B1_tail */
						if(B1_size >0){
			
						tmp->next->previous = NULL;
		 
						}
						else {B1_head = NULL;} 
			
						B1_tail = tmp->next;
						
						delete(tmp);
						replace(xt,p);
					}
	
					else {
	
						/* Delete T1_tail */
						Node *tmp = T1_tail;
						T1_hash.remove(tmp->page);
						T1_size--;
	
						tmp->next->previous = NULL;
						T1_tail = tmp->next;
						delete(tmp);
					}
					
				}
			/* Case B : If cache not filled completely */
	
				else if((T1_size+B1_size)<c){
	
					if ((T1_size+T2_size+B1_size+B2_size)>=c){
	
						if ((T1_size+T2_size+B1_size+B2_size) == 2*c) {
							
							/* Delete B2_tail */
							Node *tmp = B2_tail;
							B2_hash.remove(tmp->page);
							B2_size--;
		
							if (B2_size > 0) tmp->next->previous = NULL;
							else B2_head = NULL;
							B2_tail = tmp->next;
							delete(tmp);
						}
		
						replace(xt, p);
					}
				}
	
				/* Add a node at T1_head */
	
				Node *node = new(Node);
				node->next = NULL;
				node->previous = NULL;
			
				node->page = xt; 
				if (T1_head == NULL){
			
					T1_head = node;
					T1_tail = node;
				}
				else { 
	
					push_node(node, T1_head);
					T1_head = node;
				}
	
				T1_size++;
	
				T1_hash.add(xt, node);
	
			}
			
			page = page + 1;
		}
	}
		int hit_ratio;
		hit_ratio = hit*100000/(miss+hit); 
		if (hit_ratio%10 < 5) hit_ratio = hit_ratio/10;
		else hit_ratio = (hit_ratio/10)+1;
		//cout << "Hit ratio for ARC  is : " << hit/(miss+hit) << endl;
	//	cout << "Total no. of queries is : " << hit+miss << endl;
		cout << "Hit ratio for ARC in percentage is : " << ((float)hit_ratio)/100  << endl;
		return 0;
}
