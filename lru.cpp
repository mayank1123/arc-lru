#include <iostream>
#include <unordered_map>
#include <fstream>
// #include<bits/stdc++.h>
//#define limit 32764

using namespace std ; 

typedef struct Node {
	unsigned long int page;
	Node *next;
	Node *previous;
	} Node 	;

	/* Global head and tail*/
Node *head, *tail;
	
	/* hastable class has 3 methods */
class hashtable {
	std::unordered_map<unsigned long int, Node *> map;

public : 
	void add(unsigned long int page, Node *page_addr){
	//cout << "line 25" << endl;
		map[page] = page_addr;
	//cout << "line 27" << endl;
	}
	void remove(unsigned long int page){
	//cout << "line 29" << endl;
		map.erase(page);
	//cout << "line 31" << endl;
	}
	Node *get_addr(unsigned long int page){
	//cout << "line 35" << endl;
		try{   return map[page] ; }
		catch (exception& e) { return 0; }

	}
};


void push_front(Node *node1){
	/* Push the node in front of head */
//	cout << "line 39" << endl;
	head->next = node1;
	node1->previous = head;
	head = node1;
	head->next = NULL;
//	cout << "line 44" << endl;
}

void pop_node(Node *node){
/* Remove node from the middle */
//	cout << "line 54 " << endl;
	if(node->previous!=NULL) node->previous->next = node->next;
	if (node->next != NULL) node->next->previous = node->previous; 
	node->next = NULL;
	node->previous = NULL;
//	cout << "line 59 " <<endl;
}

void remove_node(){	
	//cout << "line 46" << endl;
	Node *tmp = tail;
	tail=tail->next;
	delete tmp;
	if(tail!=NULL) tail->previous = NULL;
	//cout << "line 52" << endl;
}

int main(int argc, char* argv[])
{
	string path;
	unsigned int limit;
	hashtable cache_hash;
	int size;
	unsigned long int page,num_of_pages,c,d;
	long long count;
	unsigned long int hit;  
	unsigned long int miss;
	int crap1 , crap2;
	int set;
	set = 1;
	size = 0;
	hit = 0;
	miss = 0;
	count =0;
	limit = 32768;
	if (argc > 2)
	{ limit = (unsigned int)atoi(argv[2]); } 

	if (argc > 2)
	{ path = argv[1];}

	/* Initialise head and tail */ 
	std::ifstream input (path);
	input >> page >> num_of_pages >> crap1 >> crap2 ; 
	
	/* Run the loop till there is data to read */
	while (input >> page >> num_of_pages >> crap1 >> crap2) { 
		while (num_of_pages){
			count ++; 
			if (set !=0){
			miss++; 
			Node *node = new Node;
			node->page = page;
			node->previous = NULL;
			node->next = NULL;
			tail = node;
			head = node;
			cache_hash.add(page,node);
			set = 0;
			num_of_pages --;
			page = page+1;
			}

			if(set == 0){
				if ( !cache_hash.get_addr(page)) {
					miss++;
					if(size < limit) 
						{
						//	cout <<"line 88" << endl;
							size+=1;
							Node *node = new Node;
							node->page = page;
							push_front(node);
							cache_hash.add(page,node);
						}

					else {
					//	cout <<"line 97" << endl;
						if (tail == NULL) cout << "error" << endl;
						cache_hash.remove(tail->page);
						remove_node();
						Node *node = new Node;
						node->page = page;
						push_front(node);
						cache_hash.add(page,node);
					//	cout << "line 124" << endl;
					}
				}
				else{
					//	cout << "line 126" << endl;
						hit++;
						if (tail->page == page && tail->next != NULL) tail = tail->next; 
						if (head->page != page) {pop_node(cache_hash.get_addr(page)); push_front(cache_hash.get_addr(page));}  
				}
			}
			set =0;
			num_of_pages--;
			page = page +1;
		}
	}
	int hit_ratio;
	hit_ratio = hit*100000/(miss+hit);
	if (hit_ratio%10 < 5) hit_ratio = hit_ratio/10;
	else hit_ratio = (hit_ratio/10)+1;
//	cout << "Hit ratio for LRU is : " << (float)(hit/(miss+hit)) << endl;
//	cout << "Total no. of queries is : " << hit+miss << endl;
	cout << "Hit ratio for LRU in percentage is : " << ((float)hit_ratio)/100 << endl;
//	cout << "Count is " << count << endl;
	return 0;
}
